<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Poppins:500&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="https://use.typekit.net/nat5lkq.css">
  <?php wp_head() ?>
</head>
