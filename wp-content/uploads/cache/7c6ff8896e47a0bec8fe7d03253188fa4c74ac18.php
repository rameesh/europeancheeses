<article <?php post_class() ?>>
  <h2 class="entry-title"><?php echo get_the_title(); ?></h2>
  <div class="spacer"></div>
  <div class="entry-content">
    <?php the_content() ?>

    <?php if(is_singular('cheeses')): ?>
    <?php
      $image = types_render_field( 'cheese-image', array() );
      $long = types_render_field( 'long-description', array() );
      $flavours = types_render_field( 'flavours-sensory-qualities', array() );
      $great = types_render_field( 'great-with' );
    ?>

<div class="row">

          <?php if(!empty($image)): ?> 
          <div class="col-lg-6">
            <?php echo $image; ?>

          </div>
          <?php endif; ?>
       

          <?php if(!empty($long)): ?> 
          <div class="col-lg-6">
            <?php echo $long; ?>

          </div>
          <?php endif; ?>
  
  </div>
  <div class="spacer"></div>
          <?php if(!empty($flavours)): ?> 
            <hr>  
            <h3>Flavours & sensory qualities</h3>
            <?php echo $flavours; ?>

          <?php endif; ?>

          <?php if(!empty($great)): ?> 
            <h3>Great with</h3>
            <?php echo $great; ?>

          <?php endif; ?>

    <?php endif; ?>
  </div>
  <footer>
    <?php echo wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

  </footer>
  <?php comments_template('/partials/comments.blade.php') ?>
</article>
