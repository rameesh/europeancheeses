<?php $__env->startSection('content'); ?>
  <?php while(have_posts()): ?> <?php the_post() ?>
    <?php echo $__env->make('partials.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.content-page', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    

    <!-- Modal -->

  <?php
  
    $rows = get_field('modal_content');
?>
    <?php if($rows): ?>

    <?php $__currentLoopData = $rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="modal fade" id="<?php echo e($row['modal_slug']); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLongTitle"><?php echo $row['modal_title']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
        <?php if($row['modal_column_01']): ?>

          <div class="col-lg-5 d-flex align-items-start flex-column">
            <?php echo $row['modal_column_01']; ?>

            
            <?php if($row['modal_button_text']): ?>
              <a href="<?php echo e($row['modal_button_link']); ?>" class="btn mt-auto"> <?php echo $row['modal_button_text']; ?></a>
            <?php endif; ?>
          </div>

        <?php endif; ?>
        <div class=" <?php if($row['modal_column_01']): ?> col-lg-7 <?php else: ?> col-lg-12 <?php endif; ?>">
          <?php echo $row['modal_column_02']; ?>

        </div>
      </div>
      </div>
      
    </div>
  </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>


  <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>