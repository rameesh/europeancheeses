<footer class="content-info">
  <div class="container">
    <div class="content">
      <?php dynamic_sidebar('sidebar-footer') ?>
      <hr class="mt-5">
      <div class="footer-logos w-100 d-block text-center pt-5"><img src="<?= App\asset_path('images/footer-logos.png'); ?>" width="512px" class="ml-auto mr-auto mt-5"></div>
    </div>
  </div>
</footer>
