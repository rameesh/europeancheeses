<article <?php post_class() ?>>
   <h3 class="entry-title"><a href="<?php echo e(get_permalink()); ?>"><?php echo get_the_title(); ?></a></h3>
    <?php if(get_post_type() === 'post'): ?>
      <?php echo $__env->make('partials/entry-meta', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
  <div class="entry-summary mb-5">
    <?php the_excerpt() ?>

    <?php
      $short = types_render_field( 'short-description', array() );
    ?>


    <?php if(!empty($short)): ?> 
            <?php echo $short; ?>

    <?php endif; ?>


  </div>
</article>
