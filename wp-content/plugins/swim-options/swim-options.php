<?php 
/**
 * Plugin Name:       SWiM Option Pages
 * Plugin URI:        http://swim.com.au
 * Description:       This plugin is used to house the code that can create options pages.
 * Version:           1.0.0
 * Author:            SWiM Communications
 * Author URI:        http://swim.com.au
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       swim-options-pages
 * Domain Path:       /languages
 */








    add_action('init', 'cloneRole');
    
    function cloneRole()
    {
        global $wp_roles;
        if ( ! isset( $wp_roles ) )
            $wp_roles = new WP_Roles();
    
        $adm = $wp_roles->get_role('administrator');
        //Adding a 'new_role' with all admin caps
        $wp_roles->add_role('new_role', 'Site Admin', $adm->capabilities);
    }





