/**
 * Declare the block you'd like to style. 
 * Duplicate and customize the four lines below for each style variation you'd like to add.
 */

wp.blocks.registerBlockStyle('core/paragraph', {
    name: 'large-paragraph',
    label: 'Large Paragraph'
});

wp.blocks.registerBlockStyle('core/paragraph', {
    name: 'large-over-image-paragraph',
    label: 'Large Over Image Paragraph'
});

wp.blocks.registerBlockStyle('core/paragraph', {
    name: 'medium-over-image-paragraph',
    label: 'Medium Over Image Paragraph'
});

wp.blocks.registerBlockStyle('core/paragraph', {
    name: 'small-over-image-paragraph',
    label: 'Small Over Image Paragraph'
});

wp.blocks.registerBlockStyle('core/paragraph', {
    name: 'small-text',
    label: 'Small text'
});

wp.blocks.registerBlockStyle('core/paragraph', {
    name: 'offset-left',
    label: 'Offset'
});

wp.blocks.registerBlockStyle('core/heading', {
    name: 'offset-left',
    label: 'Offset'
});

wp.blocks.registerBlockStyle('core/heading', {
    name: 'after-tag',
    label: '::after'
});

wp.blocks.registerBlockStyle('core/heading', {
    name: 'before-tag',
    label: '::before'
});

wp.blocks.registerBlockStyle('core/separator', {
    name: 'offset-left',
    label: 'Offset'
});

wp.blocks.registerBlockStyle('core/image', {
    name: 'margin-left-100',
    label: 'margin left'
});

wp.blocks.registerBlockStyle('core/image', {
    name: 'even-height',
    label: 'Even hight'
});

wp.blocks.registerBlockStyle('core/spacer', {
    name: 'hide-on-mobile',
    label: 'Hide on mobile'
});

wp.blocks.registerBlockStyle('core/spacer', {
    name: 'fixed-height',
    label: 'Fixed height'
});


// wp.blocks.registerBlockStyle('core/cover', {
//     name: 'intend-top',
// 	label: 'Awesome Cover'
// });