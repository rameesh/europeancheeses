<?php

/**
 * Plugin Name: SWiM Block Styles
 * Plugin URI: https://github.com/Automattic/gutenberg-block-styles/
 * Description: A simple plugin to add block styles to Gutenberg.
 * Version: 1.0
 * Author: SWiM Communications
 */

/**
 * Enqueue Block Styles Javascript
 */
function block_styles_enqueue_javascript() {
	wp_enqueue_script( 'block-styles-script',
		plugins_url( 'block.js', __FILE__ ),
		array( 'wp-blocks')
	);
}
add_action( 'enqueue_block_editor_assets', 'block_styles_enqueue_javascript' );

/**
 * Enqueue Block Styles Stylesheet
 */
function block_styles_enqueue_stylesheet() {
	wp_enqueue_style( 'block-styles-stylesheet',
		plugins_url( 'style.css', __FILE__ ) 
	);
}
add_action( 'enqueue_block_assets', 'block_styles_enqueue_stylesheet' );


// Register support for Gutenberg wide images in your theme
function mytheme_setup() {
	add_theme_support( 'align-wide' );
  }
  add_action( 'after_setup_theme', 'mytheme_setup' );

  //Disabling custom font sizes

  add_theme_support('disable-custom-font-sizes');
  add_theme_support( 'disable-custom-colors' );