@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')

    {{-- Modal Contents --}}

    <!-- Modal -->

  @php
  
    $rows = get_field('modal_content');
@endphp
    @if ($rows)

    @foreach($rows as $row)
<div class="modal fade" id="{{ $row['modal_slug']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLongTitle">{!! $row['modal_title']!!}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="row">
        @if ($row['modal_column_01'])

          <div class="col-lg-5 d-flex align-items-start flex-column">
            {!! $row['modal_column_01']!!}
            
            @if($row['modal_button_text'])
              <a href="{{ $row['modal_button_link']}}" class="btn mt-auto"> {!! $row['modal_button_text']!!}</a>
            @endif
          </div>

        @endif
        <div class=" @if ($row['modal_column_01']) col-lg-7 @else col-lg-12 @endif">
          {!! $row['modal_column_02']!!}
        </div>
      </div>
      </div>
      {{-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --}}
    </div>
  </div>
</div>
@endforeach
@endif


  @endwhile
@endsection
