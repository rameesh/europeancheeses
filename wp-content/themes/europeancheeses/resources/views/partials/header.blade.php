<header class="banner">

  <div class="container topbar d-none d-lg-block">
    <div class="col-lg-9"></div>
      <div class=" search-inline container d-flex p-0 align-items-center justify-content-end">
          <form action="{{ home_url('/') }}" role="search" method="get" class="d-flex w-50 mb-3">
            <input type="text" class="form-control" placeholder="Bon Fromage" name="s" id="s" value="<?php the_search_query(); ?>">
            <button type="submit" value="Search" class="search-icon">
            </button>
           
            </a> 
        </form>
      </div>
  </div>
  <div class="container">
   
    <nav class="nav-primary">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif

      <button class="navbar-toggler" type="button" id="menu-button">
        <span class="navbar-toggler-icon"></span>
      </button>
    </nav>

    
    <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/logo.svg')" width="360px" class="logo"></a>

    <div class="navbar-collapse nav-overlay" id="navbarSupportedContent">
      @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', container => 'false', 'menu_class' => 'navbar-nav']) !!}
      @endif 

    </div>


  </div>
</header>
