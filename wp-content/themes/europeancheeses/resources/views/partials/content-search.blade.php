<article @php post_class() @endphp>
   <h3 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
    @if (get_post_type() === 'post')
      @include('partials/entry-meta')
    @endif
  <div class="entry-summary mb-5">
    @php the_excerpt() @endphp

    @php
      $short = types_render_field( 'short-description', array() );
    @endphp


    @if (!empty($short)) 
            {!! $short !!}
    @endif


  </div>
</article>
