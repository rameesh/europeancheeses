<footer class="content-info">
  <div class="container">
    <div class="content ">
      <div class="wp-block bg-white alignwide">
       <div class="row">
          <div class="col-lg-11">
              @php dynamic_sidebar('sidebar-footer') @endphp
             </div>
             <div class="col-lg-1 social-icons px-lg-0">
              
              <a href="{{ the_field ('facebook', 'option')}}" target="_blank" class="fb"></a>
              <a href="{{ the_field ('instagram', 'option')}}" target="_blank" class="insta"></a>
      
             </div>
       </div>
      <hr class="mt-5">
      <div class="footer-logos w-100 d-block text-center pt-5"><img src="@asset('images/footer-logos@2x.png')" width="512px" class="ml-auto mr-auto mt-5"></div>
      </div>
    </div>
    <p class="copyright text-right">
        © European Cheeses 2019
    </p>
  </div>
</footer>
