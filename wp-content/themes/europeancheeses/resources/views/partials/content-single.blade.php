<article @php post_class() @endphp>
  <h2 class="entry-title">{!! get_the_title() !!}</h2>
  <div class="spacer"></div>
  <div class="entry-content">
    @php the_content() @endphp

    @if(is_singular('cheeses'))
    @php
      $image = types_render_field( 'cheese-image', array() );
      $long = types_render_field( 'long-description', array() );
      $flavours = types_render_field( 'flavours-sensory-qualities', array() );
      $great = types_render_field( 'great-with' );
    @endphp

<div class="row">

          @if (!empty($image)) 
          <div class="col-lg-6">
            {!! $image !!}
          </div>
          @endif
       

          @if (!empty($long)) 
          <div class="col-lg-6">
            {!! $long !!}
          </div>
          @endif
  
  </div>
  <div class="spacer"></div>
          @if (!empty($flavours)) 
            <hr>  
            <h3>Flavours & sensory qualities</h3>
            {!! $flavours !!}
          @endif

          @if (!empty($great)) 
            <h3>Great with</h3>
            {!! $great !!}
          @endif

    @endif
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
</article>
