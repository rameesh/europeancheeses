@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  <div class="page-header">
  <h2>{!! App::title() !!}</h2> 
  </div>
  <div class="spacer"></div>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {{-- {!! get_search_form(false) !!} --}}
  @endif

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-search')
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
