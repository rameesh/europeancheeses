@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
  <div class="alert alert-warning container  alignwide">
    <h2> {{ __('Sorry, but the page you were trying to view does not exist.', 'sage') }}</h2>
   </div>
  @endif
@endsection
