export default {
    init() {
        // JavaScript to be fired on all pages
        // // Modal Content
        // $('.pop_up a').attr({'data-toggle': 'modal'});
        // $('a[href*="#"]:not([href="#"])').attr({'data-toggle': 'modal'});
        // Mobile Menu
        $('#menu-button').on('click', function(e) {
            $('#menu-button').toggleClass('is-active'); //you can list several class names 
    
            $('body').toggleClass('is-active'); //you can list several class names 
            e.preventDefault();
        });





        // Smooth Scroll

//   $('a[href*="#"]:not([href="#"])').click(function() {
//     // console.log('click');
//     if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
//       var target = $(this.hash);
//       target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
//       // console.log(target);
//       if (target.length) {
//         // console.log(target+'exists');
//         $('html, body').animate({
//           scrollTop: target.offset().top,
//         }, 600);
//         return false;
//       }
//     }
//   });

    // ekko-Lightbox

    /**
     * Prevent default click behavior on WordPress gallery image links
     */
     /* eslint-disable */
    $(document).on('click', '.blocks-gallery-item a', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
        // showArrows: true,
        alwaysShowClose: true,
        // title: '',
        footer: '',
        maxWidth: 1280,
        maxHeight: 900,
        
        strings: {
            close: 'Close',
        
            fail: 'Failed to load image:',
        
            type: 'Could not detect remote target type. Force the type using data-type',

            },
        });
    });




   
    // Make an ekko lightbox out of WordPress galleries
    $('.blocks-gallery-item a[href]')

    // Test whether this link is directly to an image file
    .filter(function() {
    return /(jpg|gif|png)$/.test($(this).attr('href'));
    }).each(function() {
    // Get link attr
    var link = $(this).attr('href');

    // Get the image ALT text
    var alt = $(this).find('img').attr('alt');

    // Strip the file extension
    var filename = link.substr(0, $(this).attr('href').lastIndexOf('.')) || $(this).attr('href');

    // Get the index of just the filename without the path
    var fileNameIndex = filename.lastIndexOf("/") + 1;

    // Return just the filename without the path and without the extension
    filename = filename.substr(fileNameIndex);

    // Get the ALT text so we can use it as a caption ONLY if it's NOT the same as the filename!
    // WordPress automatically uses the filename as ALT text if no caption or ALT is set, so we don't want to use it if they're the same.
    var caption = (filename === alt) ? '' : alt;

    // Get the gallery ID
    var gallery = $(this).closest('.blocks-gallery-item').attr('class');

    // Add ekko lightbox data-attributes
    $(this).attr({'data-toggle': 'lightbox', 'data-gallery': gallery, 'data-footer': caption});


    });



    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};