export default {
  init() {
    // JavaScript to be fired on the home page

    // Instafeed
     /* eslint-disable */
     var Instafeed = require("instafeed.js");

     var galleryFeed = new Instafeed({
      get: "user",
      userId:4051531768,
      accessToken:'4051531768.1677ed0.7608d974f14c4fb7bf36bf4969ebb54a',
      resolution: "standard_resolution",
      sortBy: 'random',
      limit: 8,
      template: '<div class="parent"><a href="{{link}}" target="_blank"><div class="img-featured-container" style="background-image:url({{image}})"><div class="img-backdrop d-flex align-items-center justify-content-center"></div></div></a></div>',
      target: "instafeed-gallery-feed",
      after: function() {
        // disable button if no more results to load
        if (!this.hasNext()) {
          btnInstafeedLoad.setAttribute('disabled', 'disabled');
        }
      },
    });
    galleryFeed.run();
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
